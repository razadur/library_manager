<?php require_once('../include/header.php');?>

<?php require_once('../include/nav-top.php');?>

<?php require_once('../include/nav-side.php');?>
<div class="content-wrapper">
<h1>Books List</h1><a href="<?php echo $site_base_url;?>books/book-add.php">Add New Book</a>

<br><br><br>
    <table id="example" class="display" style="width:100%">
        <thead>
            <tr>
                <th>Book Name</th>
                <th>Author</th>
                <th>Genre</th>
                <th>Published Date</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Harry Potter 1 </td>
                <td>J.K.Rawling </td>
                <td>Fiction</td>
                <td>1998</td>
                <td>
                    <a href="#">View</a>
                    <a href="#">Edit</a>
                    <a href="#">Delete</a>
                </td>
            </tr>
            <tr>
                <td>Harry Potter 2 </td>
                <td>J.K.Rawling </td>
                <td>Fiction</td>
                <td>1999</td>
                <td>
                    <a href="#">View</a>
                    <a href="#">Edit</a>
                    <a href="#">Delete</a>
                </td>
            </tr>
            <tr>
                <td>Harry Potter 3 </td>
                <td>J.K.Rawling </td>
                <td>Fiction</td>
                <td>2001</td>
                <td>
                    <a href="#">View</a>
                    <a href="#">Edit</a>
                    <a href="#">Delete</a>
                </td>
            </tr>
            <tr>
                <td>Beauty and The beast </td>
                <td>Gabrielle </td>
                <td>Fiction</td>
                <td>1740</td>
                <td>
                    <a href="#">View</a>
                    <a href="#">Edit</a>
                    <a href="#">Delete</a>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<?php require_once('../include/footer.php');?>
<script>
    $(document).ready(function() {
    $('#example').DataTable();
} );
</script>