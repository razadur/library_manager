<?php require_once('../include/header.php');?>

<?php require_once('../include/nav-top.php');?>

<?php require_once('../include/nav-side.php');?>
<div class="content-wrapper">
<h1>Members List</h1><a href="<?php echo $site_base_url;?>members/members-add.php">Add New Member</a>

<br><br><br>
    <table id="example" class="display" style="width:100%">
        <thead>
            <tr>
                <th>Member Name</th>
                <th>Joining date</th>
                <th>Book taken</th>
                <th>Book return</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Laboni </td>
                <td>2021 </td>
                <td>11</td>
                <td>10</td>
                <td>
                    <a href="#">View</a>
                    <a href="#">Edit</a>
                    <a href="#">Delete</a>
                </td>
            </tr>
            <tr>
                <td>Shahriar </td>
                <td>2021 </td>
                <td>6</td>
                <td>2</td>
                <td>
                    <a href="#">View</a>
                    <a href="#">Edit</a>
                    <a href="#">Delete</a>
                </td>
            </tr><tr>
                <td>Rabby </td>
                <td>2021 </td>
                <td>7</td>
                <td>3</td>
                <td>
                    <a href="#">View</a>
                    <a href="#">Edit</a>
                    <a href="#">Delete</a>
                </td>
            </tr><tr>
                <td>Humayon </td>
                <td>2021 </td>
                <td>14</td>
                <td>12</td>
                <td>
                    <a href="#">View</a>
                    <a href="#">Edit</a>
                    <a href="#">Delete</a>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<?php require_once('../include/footer.php');?>
<script>
    $(document).ready(function() {
    $('#example').DataTable();
} );
</script>