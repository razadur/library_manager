<footer class="main-footer">
    <!-- <strong>Copyright &copy; 2014-2021 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> -->
    All rights reserved.
    <!-- <div class="float-right d-none d-sm-inline-block"> -->
      <!-- <b>Version</b> 3.1.0 -->
    </div>
  </footer>

  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="<?php echo $base_url;?>plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo $base_url;?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="<?php echo $base_url;?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo $base_url;?>plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo $base_url;?>plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo $base_url;?>plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="<?php echo $base_url;?>plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo $base_url;?>plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="<?php echo $base_url;?>plugins/jszip/jszip.min.js"></script>
<script src="<?php echo $base_url;?>plugins/pdfmake/pdfmake.min.js"></script>
<script src="<?php echo $base_url;?>plugins/pdfmake/vfs_fonts.js"></script>
<script src="<?php echo $base_url;?>plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo $base_url;?>plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo $base_url;?>plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo $base_url;?>dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo $base_url;?>dist/js/demo.js"></script>
<script src="http://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<!-- Page specific script -->

</body>
</html>