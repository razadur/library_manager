<?php require_once('../include/header.php');?>

<?php require_once('../include/nav-top.php');?>

<?php require_once('../include/nav-side.php');?>
<div class="content-wrapper">
<h1>Librarian List</h1><a href="<?php echo $site_base_url;?>librarian/librarian-add.php">Add New Librarian</a>

<br><br><br>
    <table id="example" class="display" style="width:100%">
        <thead>
            <tr>
                <th>Librarian Name</th>
                <th>Joining date</th>
                <th>Gender</th>
                <th>Librarian Address</th>
                <th>Date of birth</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Labiba </td>
                <td>2021 </td>
                <td>Female</td>
                <td></td>
                <td>1990/10/20</td>
                <td>
                    <a href="#">View</a>
                    <a href="#">Edit</a>
                    <a href="#">Delete</a>
                </td>
            </tr>
            <tr>
                <td>Habibul </td>
                <td>2020 </td>
                <td>Male</td>
                <td></td>
                <td>1988/11/12</td>
                <td>
                    <a href="#">View</a>
                    <a href="#">Edit</a>
                    <a href="#">Delete</a>
                </td>
            </tr>
            <tr>
                <td>Hasib </td>
                <td>2020 </td>
                <td>Male</td>
                <td></td>
                <td>1992/01/24</td>
                <td>
                    <a href="#">View</a>
                    <a href="#">Edit</a>
                    <a href="#">Delete</a>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<?php require_once('../include/footer.php');?>
<script>
    $(document).ready(function() {
    $('#example').DataTable();
} );
</script>